'use strict';

const ec2 = require('./ec2.js');
const sqs = require('./sqs');
const cw = require('./cw');
const s3 = require('./s3');

module.exports = {
  ec2,
  sqs,
  cw,
  s3,
};
