'use strict';

const config = require('../config.js');
const debug = require('../../debug');
const aws = require('aws-sdk');
const prepareLogsBatches = require('./utils.js').prepareLogsBatches;
const chain = require('../../async').chain;
const op = require('object-path');

const configCW = config.services.CloudWatchLogs;
const FAKE = config.other.FAKE;
const debugName = 'CWLogs';

class CWLogs {
  constructor(groupName, streamName, data) {
    this.cw = new aws.CloudWatchLogs(Object.assign(
      {},
      configCW,
      data,
      {
        apiVersion: configCW.apiVersion,
      }
    ));

    this.groupName = groupName;
    this.streamName = streamName;
  }

  init() {
    if (FAKE) {
      debug(debugName, 'fake init', this.groupName);
      return Promise.resolve();
    }

    return this.checkGroupExists(this.groupName)
    .then((res) => {
      if (res === false) {
        debug(debugName, 'Creating Group:', this.groupName);
        return this.createGroup(this.groupName);
      }
      debug(debugName, 'Group exists:', this.groupName);
      return Promise.resolve();
    })
    .then(
      () => (this.checkStreamExists(this.groupName, this.streamName))
    )
    .then((res) => {
      if (res === false) {
        debug(debugName, 'Creating Stream:', this.streamName);
        return this.createStream(this.groupName, this.streamName);
      }
      debug(debugName, 'Stream exists:', this.streamName);
      return Promise.resolve();
    });
  }

  setSequenceToken(token) {
    this.sequenceToken = token;
  }

  getSequenceToken() {
    return this.sequenceToken;
  }

  checkGroupExists() {
    return this.cw.describeLogGroups({
      logGroupNamePrefix: this.groupName,
    }).promise()
    .then(
      (res) => {
        const group = res.logGroups.filter(
          g => (g.logGroupName === this.groupName)
        )[0];

        return Promise.resolve(!!group);
      }
    );
  }

  createGroup() {
    return this.cw.createLogGroup({
      logGroupName: this.groupName,
    }).promise();
  }

  checkStreamExists() {
    return this.cw.describeLogStreams({
      logGroupName: this.groupName,
      logStreamNamePrefix: this.streamName,
    }).promise()
    .then((res) => {
      const stream = res.logStreams.filter(
        s => (s.logStreamName === this.streamName)
      )[0];
      if (stream) {
        this.setSequenceToken(stream.uploadSequenceToken);
      }
      return Promise.resolve(!!stream);
    });
  }

  createStream() {
    return this.cw.createLogStream({
      logGroupName: this.groupName,
      logStreamName: this.streamName,
    }).promise();
  }

  putLogs(data) {
    if (FAKE) {
      debug(debugName, 'putLogs fake', data);
      return Promise.resolve();
    }

    if (data.length === 0) {
      return Promise.resolve();
    }
    const batches = prepareLogsBatches(data);
    debug(debugName, 'putLogs: Start sending data');
    return chain(batches.map(b => ({
      arguments: [b],
      promise: this.putBatch.bind(this),
    })))
    .then(() => {
      debug(debugName, 'putLogs: Sending data completed');
      return Promise.resolve();
    });
  }

  putBatch(data) {
    if (FAKE) {
      debug(debugName, 'putBatch fake', data);
      return Promise.resolve();
    }

    if (data.length === 0) {
      debug(debugName, 'putBatch: No data to send!', data);
      return Promise.resolve();
    }
    debug(debugName, 'putBatch: Start sending data', data.length);
    return this.cw.putLogEvents({
      logEvents: data,
      logGroupName: this.groupName,
      logStreamName: this.streamName,
      sequenceToken: this.getSequenceToken(),
    }).promise()
    .then((res) => {
      debug(debugName, 'putBatch: Sending data completed');
      this.setSequenceToken(res.nextSequenceToken);
      return Promise.resolve();
    })
    .catch((err) => {
      debug(debugName, 'ERROR: putBatch', err.message);
      this.checkStreamExists()
      .then(() => (Promise.reject(err)));
    });
  }

  setLogs(data) {
    this.logs = data;
  }

  getLogs() {
    return this.logs;
  }

  filterLogs(pattern, startTime, endTime) {
    debug(debugName, 'filterLogs: Invoked..');
    const params = {
      logGroupName: this.groupName,
      filterPattern: pattern,
      startTime,
      endTime,
      nextToken: this.getSequenceToken(),
    };
    return this.cw.filterLogEvents(params)
    .promise()
    .then((data) => {
      const logs = this.getLogs();
      if (op.get(logs, 'events')) {
        data.events.concat(logs.events);
      }
      if (data.nextToken) {
        debug(debugName, 'filterLogs: Read logs from the remaining stream');
        this.setSequenceToken(data.nextToken);
        this.setLogs(data);
        return this.filterLogs(pattern, startTime, endTime);
      } else {
        debug(debugName, 'filterLogs: Starting Filtering of logs..');
        this.setSequenceToken(null);
        if (data.events.length > 0) {
          return Promise.resolve(data.events.map((x) => {
            let out;
            try {
              out = JSON.parse(x.message);
            } catch (e) {
              out = x.message;
            }
            return out;
          }));
        }
        return Promise.resolve();
      }
    })
    .catch((error) => {
      debug(debugName, 'ERROR: filterLogs', error.message);
      return Promise.reject(error);
    });
  }
}

module.exports = {
  CWLogs,
};
