'use strict';

const debug = require('../debug');
const getId = require('../id').getId;

const debugName = 'TRANSACTION';
const SYSTEM_ERROR = 'SYSTEM_ERROR';
let index = 0;

const getTransactionId = (serviceName) => {
  debug(debugName, '\tGot Service Name ', serviceName);
  return (req, res, next) => {
    getId(serviceName)
    .then((ec2Id) => {
      debug(debugName, '\tAdding TRANSACTION ID to response headers');
      res.setHeader('X-Request-ID', `${ec2Id}-${index += 1}`);
      next();
    })
    .catch((err) => {
      debug(debugName, '\tError in transactionID MW - ', err);
      next(SYSTEM_ERROR);
    });
  };
};

module.exports = {
  getTransactionId,
};
