'use strict';

const getBase = require('./base.js').getLogTemplate;
const config = require('./config.js').getConfig();

const KEYS = config.KEYS;
const VALUES = config.VALUES;

function getInfo(data) {
  const base = getBase();

  const infoAttrs = {};
  infoAttrs[KEYS.TYPE] = VALUES.TYPE.INFO;
  infoAttrs[KEYS.MESSAGE] = data;

  return Object.assign(
    {},
    base,
    infoAttrs
  );
}

module.exports = {
  getInfo,
};
