'use strict';

const PD = require('probability-distributions');
const debug = require('../debug');
const config = require('./config');
const validation = require('./validation');

const debugName = 'LATENCY';
let index = 0;
let latencyFlag = false;
let distributionList = [];

const addLatency = (latencyConfig) => {
  debug('CREATING SAMPLE VALUE FOR - ', JSON.stringify(latencyConfig));
  latencyFlag = true;
  let list;
  const minLatency = Number(latencyConfig.minLatency);
  const maxLatency = Number(latencyConfig.maxLatency);
  const samples = Number(latencyConfig.samples);
  const type = config.distributionType[latencyConfig.distribution];
  if (type === 'rbinom' || type === 'rnorm')
    list = PD[type](samples, Math.ceil((minLatency + maxLatency) / 2), 0.3);
  else if (type === 'Linear')
    list = Array(samples).fill(minLatency)
  else
    list = PD[type](samples, minLatency, maxLatency);
  distributionList = list.map((value) => {
    if (value < minLatency)
      value += minLatency;
    return Math.min(value, maxLatency);
  });
  debug('DISTRIBUTION LIST CREATED - ', JSON.stringify(distributionList));
  index = 0;
};

const deleteLatecy = () => {
  debug('REMOVING LATENCY EFFECT');
  latencyFlag = false;
  distributionList = [];
  index = 0;
};

const applyLatency = (req, res, next) => {
  if (!latencyFlag) {
    debug('ADD LATENCY DISABLED');
    next();
  } else {
    debug('ADD LATENCY ENABLED');
    index = index === distributionList.length - 1 ? 0 : index += 1;
    const wait = distributionList[index];
    debug('ADDING LATENCY OF ', wait, 'ms');
    setTimeout(next, wait);
  }
};


module.exports = {
  config,
  validation,
  applyLatency,
  deleteLatecy,
  addLatency,

};

