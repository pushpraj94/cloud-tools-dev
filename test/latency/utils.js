'use strict';

const payload = (min, max, type, samples) => {
  return {
    minLatency: min,
    maxLatency: max,
    distribution: type,
    samples,
  };
};

module.exports = {
  payload,
}
