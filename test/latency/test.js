'use strict';

const expect = require('chai').expect;
const latencyUtils = require('../../source/latency');
const utils = require('./utils');

const req = {};
const res = {};

describe('Test Latency Utils', function() {
  this.timeout(10000);
  describe('Test Validation', function () {
    it('has to be a function', function() {
      expect(latencyUtils.validation.validateSchema).to.be.a('function');
    });

    it('String is passed as min & max value in payload', function () {
      const payload = utils.payload('10', '25', 'Uniform', 100);
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"minLatency" references "maxLatency" which is not a number');
    });

    it('Negative number is passed as min value in payload', function () {
      const payload = utils.payload(-1, 25, 'Uniform', 100);
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"minLatency" must be larger than or equal to 0');
    });

    it('min value is greater than max value in payload', function () {
      const payload = utils.payload(100, 50, 'Uniform', 100);
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"minLatency" must be less than or equal to 50');
    });

    it('Invalid distribution type is passed in payload', function () {
      const payload = utils.payload(1, 10, 'test', 100);
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"distribution" must be one of [Uniform, Poisson, Beta, Gamma, Binomial, Normal, Linear, F-Distribution]');
    });

    it('samples field is missing in payload', function () {
      const payload = {
        minLatency: 200,
        maxLatency: 300,
        distribution: 'Linear',
      };
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"samples" is required');
    });

    it('minLatency field is missing in payload', function () {
      const payload = {
        maxLatency: 300,
        distribution: 'Linear',
        samples: 100,
      };
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"minLatency" is required');
    });

    it('distribution field is missing in payload', function () {
      const payload = {
        minLatency: 200,
        maxLatency: 300,
        samples: 100,
      };
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"distribution" is required');
    });

    it('unknown field is present in payload', function () {
      const payload = {
        minLatency: 200,
        maxLatency: 300,
        distribution: 'Linear',
        samples: 100,
        type: 'dafg',
      };
      const errObj = latencyUtils.validation.validateSchema(payload);
      expect(errObj).to.not.be.null;
      expect(errObj.details[0].message).to.be.equal('\"type" is not allowed');
    });
  });

  describe('Test applyLatency', function() {
    it('has to be a function', function() {
      expect(latencyUtils.applyLatency).to.be.a('function');
    });

    it('Latency Configuration is not provided - no latency should be added', function() {
      latencyUtils.deleteLatecy();
      const ts_a = (new Date()).getTime();
      latencyUtils.applyLatency(req, res, () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(0);
        expect(ts_b - ts_a).to.be.at.most(5);
        latencyUtils.applyLatency(req, res, () => {
          const ts_c = (new Date()).getTime();
          expect(ts_c - ts_b).to.be.at.least(0);
          expect(ts_c - ts_b).to.be.at.most(5);
        });
      });
    });
  });

  describe('Test addLatency', function() {
    it('has to be a function', function() {
      expect(latencyUtils.addLatency).to.be.a('function');
    });

    it('Adding Linear latency - delay should be minLatency provided', function() {
      const latencyConfig = {
	      minLatency: 200,
	      maxLatency: 500,
        distribution: 'Linear',
        samples: 100,
      };
      latencyUtils.addLatency(latencyConfig);
      const ts_a = (new Date()).getTime();
      latencyUtils.applyLatency(req, res, () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(200);
        expect(ts_b - ts_a).to.be.at.most(230);
        latencyUtils.applyLatency(req, res, () => {
          const ts_c = (new Date()).getTime();
          expect(ts_c - ts_b).to.be.at.least(200);
          expect(ts_c - ts_b).to.be.at.most(230);
          latencyUtils.applyLatency(req, res, () => {
            const ts_d = (new Date()).getTime();
            expect(ts_d - ts_c).to.be.at.least(200);
            expect(ts_d - ts_c).to.be.at.most(230);
            latencyUtils.applyLatency(req, res, () => {
              const ts_e = (new Date()).getTime();
              expect(ts_e - ts_d).to.be.at.least(200);
              expect(ts_e - ts_d).to.be.at.most(230);
            });
          });
        });
      })
    });

    it('Adding Uniform latency', function() {
      const latencyConfig = {
	      minLatency: 150,
	      maxLatency: 300,
        distribution: 'Uniform',
        samples: 100,
      };
      latencyUtils.addLatency(latencyConfig);
      latencyUtils.applyLatency(req, res, () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(150);
        expect(ts_b - ts_a).to.be.at.most(320);
        latencyUtils.applyLatency(req, res, () => {
          const ts_c = (new Date()).getTime();
          expect(ts_c - ts_b).to.be.at.least(150);
          expect(ts_c - ts_b).to.be.at.most(320);
          latencyUtils.applyLatency(req, res, () => {
            const ts_d = (new Date()).getTime();
            expect(ts_d - ts_c).to.be.at.least(150);
            expect(ts_d - ts_c).to.be.at.most(320);
            latencyUtils.applyLatency(req, res, () => {
              const ts_e = (new Date()).getTime();
              expect(ts_e - ts_d).to.be.at.least(150);
              expect(ts_e - ts_d).to.be.at.most(320);
            });
          });
        });
      })
    });

    it('Adding Beta latency', function() {
      const latencyConfig = {
	      minLatency: 200,
	      maxLatency: 250,
        distribution: 'Beta',
        samples: 100,
      };
      latencyUtils.addLatency(latencyConfig);
      latencyUtils.applyLatency(req, res, () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(200);
        expect(ts_b - ts_a).to.be.at.most(260);
        latencyUtils.applyLatency(req, res, () => {
          const ts_c = (new Date()).getTime();
          expect(ts_c - ts_b).to.be.at.least(200);
          expect(ts_c - ts_b).to.be.at.most(260);
          latencyUtils.applyLatency(req, res, () => {
            const ts_d = (new Date()).getTime();
            expect(ts_d - ts_c).to.be.at.least(200);
            expect(ts_d - ts_c).to.be.at.most(260);
            latencyUtils.applyLatency(req, res, () => {
              const ts_e = (new Date()).getTime();
              expect(ts_e - ts_d).to.be.at.least(200);
              expect(ts_e - ts_d).to.be.at.most(260);
            });
          });
        });
      })
    });

    it('Adding Gamma latency', function() {
      const latencyConfig = {
	      minLatency: 200,
	      maxLatency: 220,
        distribution: 'Gamma',
        samples: 100,
      };
      const next = () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(200);
        expect(ts_b - ts_a).to.be.at.most(230);
      };
      latencyUtils.addLatency(latencyConfig);
      latencyUtils.applyLatency(req, res, () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(200);
        expect(ts_b - ts_a).to.be.at.most(230);
        latencyUtils.applyLatency(req, res, () => {
          const ts_c = (new Date()).getTime();
          expect(ts_c - ts_b).to.be.at.least(200);
          expect(ts_c - ts_b).to.be.at.most(230);
          latencyUtils.applyLatency(req, res, () => {
            const ts_d = (new Date()).getTime();
            expect(ts_d - ts_c).to.be.at.least(200);
            expect(ts_d - ts_c).to.be.at.most(230);
            latencyUtils.applyLatency(req, res, () => {
              const ts_e = (new Date()).getTime();
              expect(ts_e - ts_d).to.be.at.least(200);
              expect(ts_e - ts_d).to.be.at.most(230);
            });
          });
        });
      })
    });

    it('Adding Poisson latency', function() {
      const latencyConfig = {
	      minLatency: 200,
	      maxLatency: 300,
        distribution: 'Poisson',
        samples: 100,
      };
      const next = () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(200);
        expect(ts_b - ts_a).to.be.at.most(310);
      };
      latencyUtils.addLatency(latencyConfig);
      latencyUtils.applyLatency(req, res, () => {
        const ts_b = (new Date()).getTime();
        expect(ts_b - ts_a).to.be.at.least(200);
        expect(ts_b - ts_a).to.be.at.most(310);
        latencyUtils.applyLatency(req, res, () => {
          const ts_c = (new Date()).getTime();
          expect(ts_c - ts_b).to.be.at.least(200);
          expect(ts_c - ts_b).to.be.at.most(310);
          latencyUtils.applyLatency(req, res, () => {
            const ts_d = (new Date()).getTime();
            expect(ts_d - ts_c).to.be.at.least(200);
            expect(ts_d - ts_c).to.be.at.most(310);
            latencyUtils.applyLatency(req, res, () => {
              const ts_e = (new Date()).getTime();
              expect(ts_e - ts_d).to.be.at.least(200);
              expect(ts_e - ts_d).to.be.at.most(310);
            });
          });
        });
      })
    });
  });
});