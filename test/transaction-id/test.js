'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const path = require('path');
const decache = require('decache');
const httpMocks = require('node-mocks-http');

const transactionIdPath = path.join(__dirname, '../../source/transaction-id/index.js');
const ec2Path = path.join(__dirname, '../../source/aws/ec2.js');

const serviceName = 'DUMMY_APP';
const environmentName = 'DUMMY_ENV'
const fakeEC2Id = '123';
const fakeTime = '2017-08-29T16:33:56Z';
const fakeTS = (new Date(fakeTime)).getTime();
const pid = process.pid;
let req, res;

describe('Transaction-ID Unit test cases', function() {
  beforeEach(function() {
    decache(ec2Path);
    decache(transactionIdPath);
    req = httpMocks.createRequest();
    res = httpMocks.createResponse();
  });

  afterEach(function() {
    decache(ec2Path);
    decache(transactionIdPath);
  });

  it('should have a getTransactionId function', function(done) {
    const getTransactionId = require(transactionIdPath).getTransactionId;
    expect(getTransactionId).to.be.a('function');
    done();
  });

  it('should have X-Request-ID in response header when getTransactionId is called', sinon.test(function(done) {
    this.clock = sinon.useFakeTimers(fakeTS);
    const awsStub = this.stub(require(ec2Path), 'getInstanceId', () => (Promise.resolve(fakeEC2Id)));

    const getTransactionId = require(transactionIdPath).getTransactionId;
    let index = 0;

    getTransactionId(serviceName)(req, res, (error) => {
      if (error) {
        return done(new Error('Expected not to receive an error'));
      }
      expect(res.getHeader('X-Request-ID')).to.be.equal(`${serviceName}-${fakeEC2Id}-${pid}-${fakeTS}-${index += 1}`);
      expect(index).to.be.equal(1);
      return done();
    })
  }));

  it('should increment the X-Request-ID value when getTransactionId is called multiple times', sinon.test(function(done) {
    this.clock = sinon.useFakeTimers(fakeTS);
    const awsStub = this.stub(require(ec2Path), 'getInstanceId', () => (Promise.resolve(fakeEC2Id)));

    const getTransactionId = require(transactionIdPath).getTransactionId;
    let index = 0;
    
    Promise.resolve()
    .then(() => {
      getTransactionId(serviceName)(req, res, (error) => {
        if (error) {
          throw new Error('Expected not to receive an error');
        }
        expect(res.getHeader('X-Request-ID')).to.be.equal(`${serviceName}-${fakeEC2Id}-${pid}-${fakeTS}-${index += 1}`);
        expect(index).to.be.equal(1);
        return;
      });
    })
    .then(() => {
      getTransactionId(serviceName)(req, res, (error) => {
        if (error) {
          throw new Error('Expected not to receive an error');
        }
        expect(res.getHeader('X-Request-ID')).to.be.equal(`${serviceName}-${fakeEC2Id}-${pid}-${fakeTS}-${index += 1}`);
        expect(index).to.be.equal(2);
        return done();
      });
    })
    .catch(done);
  }));

  it('should return error when getInstanceId returns error', sinon.test(function(done) {
    this.clock = sinon.useFakeTimers(fakeTS);
    const awsStub = this.stub(require(ec2Path), 'getInstanceId', () => (Promise.reject('EC2 unreachable')));

    const getTransactionId = require(transactionIdPath).getTransactionId;
    let index = 0;

    getTransactionId(serviceName)(req, res, (error) => {
      if (error) {
        expect(error).to.be.equal('SYSTEM_ERROR');
        expect(res.getHeader('X-Request-ID')).to.be.undefined;
        expect(index).to.be.equal(0);
        return done();
      }
      return done(new Error('Expected not to receive an error'));
    });
  }));
});
