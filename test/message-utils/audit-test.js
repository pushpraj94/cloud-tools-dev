'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const path = require('path');

const indexPath = path.join(__dirname, '../../source/message-utils/index.js');
const auditPath = path.join(__dirname, '../../source/message-utils/audit.js');
const configPath = path.join(__dirname, '../../source/message-utils/config.js');
const basePath = path.join(__dirname, '../../source/message-utils/base.js');
const idPath = path.join(__dirname, '../../source/id/index.js');
const awsPath = path.join(__dirname, '../../source/aws/index.js');
const ec2Path = path.join(__dirname, '../../source/aws/ec2.js');

const dummyName = 'dummy_name';
const dummyEnv = 'dummy_env';
const dummyAuditType = 'SMDP';
const dummyMessage = 'This a test message!!';
const dummyId = '123';

describe('MessageUtils Audit message Tests', function() {
  beforeEach(function() {
    delete require.cache[indexPath];
    delete require.cache[configPath];
    delete require.cache[auditPath];
    delete require.cache[basePath];
    delete require.cache[idPath];
    delete require.cache[awsPath];
    delete require.cache[ec2Path];
  });

  afterEach(function() {
    delete require.cache[indexPath];
    delete require.cache[configPath];
    delete require.cache[auditPath];
    delete require.cache[basePath];
    delete require.cache[idPath];
    delete require.cache[awsPath];
    delete require.cache[ec2Path];
  });

  it('should have a getAudit function', function() {
    const getAudit = require(indexPath).getAudit;
    expect(getAudit).to.be.a('function');
  });

  it('if called without initialized config throws an error', function() {
    const getAudit = require(indexPath).getAudit;
    expect(getAudit).to.throw(Error);
  });

  it('should have the correct attributes', sinon.test(function(done) {
    this.stub(require('../../source/aws/ec2.js'), 'getInstanceId', () => (
      Promise.resolve(dummyId)
    ));

    const init = require(indexPath).init;
    const getAudit = require(indexPath).getAudit;

    init(dummyName, dummyEnv)
    .then(() => {
      const msg = getAudit(dummyAuditType, dummyMessage);
      expect(msg.TIME_STAMP).to.be.an('number');
      expect(msg).to.have.property('ENVIRONMENT_NAME', dummyEnv);
      expect(msg.INSTANCE_ID.indexOf(dummyName)).to.be.equal(0);
      expect(msg).to.have.property('LOG_TYPE', 'AUDIT');
      expect(msg).to.have.property('AUDIT_TYPE', dummyAuditType);
      expect(msg).to.have.property('MESSAGE', dummyMessage);
      done();
    })
    .catch(done);
  }));

  it('if auditType isnt provided it throws an error', sinon.test(function(done) {
    this.stub(require('../../source/aws/ec2.js'), 'getInstanceId', () => (
      Promise.resolve(dummyId)
    ));

    const init = require(indexPath).init;
    const getAudit = require(indexPath).getAudit;

    init(dummyName, dummyEnv)
    .then(() => {
      expect(getAudit).to.throw(Error);
      done();
    })
    .catch(done);
  }));

  it('has getCommonAuditTypes function', function() {
    const getCommonAuditTypes = require(indexPath).getCommonAuditTypes;
    expect(getCommonAuditTypes).to.be.a('function');
  });

  it('getCommonAuditTypes returns the correct attributes', function() {
    const getCommonAuditTypes = require(indexPath).getCommonAuditTypes;
    const auditTypes = getCommonAuditTypes();
    expect(auditTypes).to.have.property('SMDP', 'SMDP');
    expect(auditTypes).to.have.property('LEAP_API', 'LEAP_API');
  });
});
